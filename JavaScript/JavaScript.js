$( document ).ready(function() {
	var navOffset = $('.navigation').offset().top;
	$(window).scroll(function(){
	    var scrolled = $(this).scrollTop();
	    if (scrolled > navOffset) {
	    	$('#header').addClass('nav__fixed');
	    }
	    else if(scrolled < navOffset) {
	    	$('#header').removeClass('nav__fixed');
	    }
	});

	$('a.fullscreen__link').click(function(){
		$('html, body').animate({
			scrollTop: $($(this).attr('href')).offset().top + 'px'
		}, {
			duration: 2000,
			easing: 'swing'
		});
	});

	$('.navigation__burger').on('click', function(){
        $('.navigation__burger-menu').slideToggle(800);
        $('.navigation__burger,.navigation__burger-menu').toggleClass('active');
    });
});